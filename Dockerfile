FROM php:7.2-apache

# Set the locale
RUN apt-get clean && apt-get -y update && apt-get install -y locales && locale-gen en_US.UTF-8

# Set environment variables
ENV COMPOSER_ALLOW_SUPERUSER=1

# Install Ubuntu packages
RUN apt-get update && apt-get install -y \
        build-essential \
        curl \
        git \
        gnupg2 \
        libcurl4-openssl-dev \
        libicu-dev \
        libmcrypt-dev \
        libxml2-dev \
        default-mysql-client \
        default-libmysqlclient-dev \
        libpng-dev \
        ruby \
        software-properties-common \
        vim \
        zip


# Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get install -y nodejs

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install curl intl json pdo_mysql mbstring zip exif pcntl soap
RUN docker-php-ext-configure gd
RUN docker-php-ext-install gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Redis ext
RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis

# Apache configuration
RUN a2enmod rewrite ssl
RUN service apache2 restart

# Change uid and gid of apache to docker user uid/gid
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

# Install package dependencies
WORKDIR /var/www/html